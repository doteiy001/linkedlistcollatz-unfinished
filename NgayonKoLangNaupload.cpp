#include<iostream>
#define MAX 1000
using namespace std;

class List{
	private:
		typedef struct node{
			int data;
			node* next;
		}* nodePtr;
		nodePtr head;
		nodePtr curr;
		nodePtr temp;
	
	public:
		List();
		void Add(int addData);
		void Delete(int delData);
		void Print();
		void Edit(int data, int newData);
		int Find(int Data);
		
};

List::List(){
	head = NULL;
	curr = NULL;
	temp = NULL;
}

void List::Add(int addData){
	nodePtr n = new node;
	n->next = NULL;
	n->data = addData;
	if(head != NULL){
		curr = head;
		while (curr->next != NULL){
			curr = curr->next;
		}
		curr->next = n;
	}
	else{
		head = n;
	}
}

void List::Delete(int delData){
	nodePtr delPtr = NULL;
	temp = head;
	curr = head;
	while (curr != NULL && curr->data != delData){
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL){
		cout<<delData<<" was not in the List\n";
	}
	else if (curr==head){
		head=head->next;
	}
	else{
		delPtr = curr;
		curr = curr->next;
		temp->next = curr;
	}
	 //cout<<"The Value "<< delData<<"was deleted\n";
	delete delPtr;
}

void List::Print(){
	curr = head;
	while (curr != NULL){
		cout<<curr->data<<" ";
		curr = curr->next;
	}
}
//at a specified index
void List::Edit(int data, int newData){
	curr = head;
	while (curr->data != data){
		curr = curr->next;
	}
	if (curr){
		curr->data = newData;
	}
}

int List::Find(int Data){
	curr = head;
	int currIndex = 1;
	while (curr && curr->data != Data){
		curr = curr->next;
		currIndex++;
	}
	if (curr){
		return currIndex ;
	}
	return 0;
}

List sorted;

int * collatzGenerator(int c){
	static int arr[MAX] = {0};
	int count=0;
		while (c>1)
		{
		    if((c%2)==0)
		    {
		        c/=2;
		        cout<<c<<" ";
		        arr[count] = c;
		    }
		    else
		    {
		        c=3*c+1;
				cout<<c<<" ";
				arr[count] = c;
			}
		    count ++;
		}
	cout<<endl;
	return arr;
}


int sorter(int arr[]){
	int *p;
	int c,temp,j;
	p = collatzGenerator(c);
	for(c;c<MAX;c++)
    {
        for(int j=0;j<MAX;j++)
        {
            if(*(p+c)>*(p+j))
            {
                temp=p[c];
                p[c]=p[j];
                p[j]=temp;
            }
        }
    }
    cout<<"Ascending Series : ";
    for(int i=MAX;i>-1;i--)
    {
        if(p[i]!=0){
        	sorted.Add(p[i]);
		}
    }
    sorted.Print();
    return c;
}

int palindromeCheck(int c){
	int rev=0, val;
	val = c;
	while (c>0){
		rev = rev*10+c%10;
		c = c/10;
	}
	if (val==rev){
		//cout<<"Palindrome: "<<val<<endl;
		return val;
	}
	else{
		return 0;
	}
}

int *palindromeGenerator(int *a)
{
	int foo, boo;
	static int temp[MAX];
	static int palindrome[MAX];
	for (int i=0; i<=MAX; i++){
		if (*(a+i)!=0){
			temp[i]=*(a+i);
			//cout<<temp[i]<<endl;
		}
		if (temp[i]!=0){
			foo = temp[i];
			boo = palindromeCheck(foo);
			palindrome[i]=boo;
			//break;
		}
	}
	return palindrome;
}

static long long unsigned int *fibonacciGenerator(int c,int arr[]){
	int *p;
	p = collatzGenerator(c);
	static long long unsigned int fiboArr[90];
	static long long unsigned int n, t1 = 0, t2 = 1,i1;
    for (i1 = 0; i1 < 92; ++i1)
    {
        if(i1 == 0){
        	continue;
		}
        else if(i1== 1){
            continue;
        }
        else if(i1 == 2){
            continue;
        }
        fiboArr[i1] = t1 + t2;
        t1 = t2;
        t2 = fiboArr[i1];
        if(fiboArr[i1]!=*(p+i1)){
		
		}
    }
    
    for(int x = 91; x > 0; x--){
    	cout<<x-1<<": ";
        cout << fiboArr[x] <<endl;
	}
}

int switchHandler(int choice){
	system("CLS");
	cout<<"Current List is : ";
		sorted.Print();
		cout<<endl;
		cout<<"1.) Add an Element to the List\n2.) Delete an Element from the List\n3.) Check whether an Element exists in the List.\n4.) Edit an Existing Element from the List.\n5.) Print the Current List.\n";
		cout<<"What Would you like to do? > ";
		cin>>choice;
		switch(choice){
			case 1: //add
				int add; 
				cout<<"Enter Element to be Added to the List > ";
				cin>>add;
				if(cin.fail()){
					cout<<"Invalid Input.\n";
				}
				sorted.Add(add);
				cout<<"Current List is : ";
				sorted.Print();
				cout<<endl;
			break;
			case 2: //delete
				int del;
				cout<<"Enter Element to be Deleted from the List > ";
				cin>>del;
				if(cin.fail()){
					cout<<"Invalid Input.\n";
				}
				sorted.Delete(del);
				cout<<"Current List is : ";
				sorted.Print();
				cout<<endl;
			break;
			case 3: //find
			{
				int find;
				cout<<"Enter Number to be Searched from the List > ";
				cin>>find;
				sorted.Find(find);
				if(cin.fail()){
					cout<<"Invalid Input.\n";
				}
				int index = sorted.Find(find);
				if(index!=0){
					cout<<"The data "<<find<<" is in the "<<index<<" position"<<endl;
				}
				else{
					cout<<"The data "<<find<<" doesn't exist in this list.\n";	
				}
			}
			break;
			case 4: //edit
			{
				int edit;
				cout<<"Enter Element you want to edit > ";
				cin>>edit;
				sorted.Find(edit);
				if(cin.fail()){
					cout<<"Invalid Input.\n";
					switchHandler(choice);
				}else{
					int editIndex = sorted.Find(edit);
					if(editIndex!=0){
						int nData;
						cout<<"Enter New Data > ";
						cin>>nData;
						sorted.Edit(edit,nData);
						sorted.Print();
						cout<<endl;
					}else{
						cout<<"The Element "<<edit<<" doesn't exist in the list. \n";
					}
				}
			}
			break;
			case 5://print
				cout<<"Current List is : ";
				sorted.Print();
				cout<<endl;
			break;
			default:
				cout<<"Invalid Input.\n";
			break;
			if(cin.fail()){
				cout<<"Invalid Input.\n";
			}
		}
		system("pause");
		return switchHandler(choice);
}

int main(){
	int c,choice;
	cout<<"Enter Number > ";
	cin>>c;
	if(c >= 2){
	int *p,*p1;
	static long long unsigned int *f;
	int a,j,temp=0,i;
	cout<<"Sequence : ";
	static int tempArr[MAX];
	p = collatzGenerator(c);
	int count;
	cout<<"My Array : ";
	for(a=0 ; a < MAX; a++){
			if(*(p+a)!=0){
				cout<<*(p+a)<<" ";
			}
			else{
				break;
			}
		}
	sorter(p);
	cout<<endl;
	p1 = palindromeGenerator(p);
	cout<<"Palindromes : ";
	for (int i=0; i<=MAX; i++){
		if (*(p1+i)!=0){
			cout<<*(p1+i)<<" ";
			tempArr[i]=*(p1+i);
		}
	}
	cout<<endl;
	char yes;
	cout<<"Display Fibonacci Series?(y/n) > ";
	cin>>yes;
	if(yes=='y'|| yes=='Y'){
		cout<<"Fibonacci Sequence : \n";
		f = fibonacciGenerator(c,p);
		system("pause");
		switchHandler(choice);
		char stay;
		cout<<"\nWould you like to stay? (y/n) > ";
		cin>>stay;
		if(stay=='y'||stay=='Y'){
			return switchHandler(choice);
		}
		else{
			return 0;
		}
	}
	else{
		cout<<"Thank you...\n";
	}
	}
	else{
		cout<<"0 and 1 are not allowed.\n";
	}
}